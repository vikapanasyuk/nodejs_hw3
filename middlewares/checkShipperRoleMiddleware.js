const checkShipperRoleMiddleware = async (req, res, next) => {
  const { role } = req.user;

  if (role === 'DRIVER') {
    return next({
      message: "You haven't access",
      status: 400
    });
  }
  next();
}

module.exports = checkShipperRoleMiddleware;