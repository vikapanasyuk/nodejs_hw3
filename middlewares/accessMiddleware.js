const accessMiddleware = async (req, res, next) => {

  const { _id, role } = req.user;

  if (role === 'DRIVER') {
    if (_id !== req.truck.created_by) {

      return next({
        message: "You haven't access",
        status: 403
      });
    }
  } else  {
    if (_id !== req.load.created_by) {

      return next({
        message: "You haven't access",
        status: 403
      });
    }
  }
  next();
}

module.exports = accessMiddleware;