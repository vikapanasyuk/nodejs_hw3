const checkDriverRoleMiddleware = async (req, res, next) => {
  const { role } = req.user;

  if (role === 'SHIPPER') {
  return next({
      message: "You haven't access",
      status: 400
    });
  }
  next();
}

module.exports = checkDriverRoleMiddleware;