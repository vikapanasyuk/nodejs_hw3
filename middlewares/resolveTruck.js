const Truck = require('../models/truck')

const resolveTruck = async (req, res, next, id) => {
  try {
      const truck = await Truck.findById(id);

      if (!truck) {
          return next({
              message: "Truck is not found",
              status: 404
          });
      }
      req.truck = truck; 
      next();
  } catch (err) {
      next(err);
  }
}

module.exports = resolveTruck;