const Load = require('../models/load')

const resolveLoad = async (req, res, next, id) => {
  try {
      const load = await Load.findById(id);

      if (!load) {
          return next({
              message: "Truck is not found",
              status: 404
          });
      }
      req.load = load; 
      next();
  } catch (err) {
      next(err);
  }
}

module.exports = resolveLoad;