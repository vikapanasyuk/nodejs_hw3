const errorHandlerMiddleware = (err, req, res, next) => {

  const { status = 500, message = 'Internal server error' } = err;
  res.status(status).send({ message })
}

module.exports = errorHandlerMiddleware;