const Load = require('../models/load');
const Truck = require('../models/truck');
const truckTypes = require('../data/truckTypes')
const loadState = require('../data/loadState')

const getLoads = async (req, res, next) => {

  try {
    const { _id, role } = req.user;
    let loads;

    if (role === 'SHIPPER') {
      loads = await Load.find({created_by: _id});
    } else {
      loads = await Load.find({assigned_to: _id});
    }

    if (!loads.length) {
      return next({
        message: "No loads found",
        status: 404
      });
    }

    res.json([{loads}]);

  } catch (err) {
    next(err);
  }
}

const addLoads = async (req, res, next) => {

  try {
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    const { _id } = req.user;

    const log = [
      {
        message: `Load created by shipper with id ${ _id }`,
        time: new Date().toISOString()
      }
    ];

    const load = new Load({created_by: _id, name, payload, pickup_address, delivery_address, dimensions, logs: log, created_date: new Date().toISOString() });
    await load.save();

    res.json({message: "Load created successfully"});

  } catch (err) {
    next(err);
  }
}

const getActiveLoad = async (req, res, next) => {

  try {
    const { _id } = req.user;
    
    const activeLoad = await Load.findOne({assigned_to: _id});

    if (!activeLoad) {
      return next({
        message: "No load found",
        status: 404
      });
    }

    res.json({activeLoad});

  } catch (err) {
    next(err);
  }
}

const changeActiveLoadsState = async (req, res, next) => {

  try {
    const { _id } = req.user;

    const load = await Load.findOne({ assigned_to: _id });
    const truck = await Truck.findOne({ assigned_to: _id });

    if (!load || !truck) {
      return next({
        message: 'No active load found',
        status: 404
      })
    }

    const status = load.status;

    if (status === 'SHIPPED') {
      return next({
        message: 'Load has already been shipped',
        status: 400
      })
    }

    const stateIndex = loadState.findIndex(state => state === load.state);
    const state = loadState[stateIndex + 1];

    const log = {
      message: `Load changed state to ${ state }`,
      time: new Date().toISOString()
    };

    if (state === 'Arrived to delivery') {
      await load.updateOne({ state, status: 'SHIPPED', $push: { logs: log} });
      await truck.updateOne({ status: 'IS' });
    } else {
      await load.updateOne({ state, $push: { logs: log} });
    }

    res.json({message: `Load state changed to '${state}'`});

  } catch (err) {
    next(err);
  }
}

const getLoadById = async (req, res, next) => {

  try {
    const { _id, role } = req.user;
    const load = req.load;
    const createdByShipper = load.created_by;
    const assignedToDriver = load.assigned_to;

    if (role === 'SHIPPER') {
      if (_id === createdByShipper) {
        res.json({load})
      } else {
        next({
          message: 'You haven\'t created the load',
          status: 400
        })
      }
    } else {
      if (_id === assignedToDriver) {
        res.json({load})
      } else {
        next({
          message: 'You haven\'t assigned the load',
          status: 400
        })
      }
    }
  } catch (err) {
    next(err);
  }
}

const updateLoadById = async (req, res, next) => {

  try {
    const load = req.load;
    const { status } = load;
    const { ...props } = req.body;

    if (status !== 'NEW') {
      return next({
        message: `You can't update load with status ${status}`
      })
    }

    const log = {
      message: `Load was updated`,
      time: new Date().toISOString()
    };

    await load.updateOne({$set: props, $push: {logs: log}});
    res.json({message: "Load details changed successfully"});

  } catch (err) {
    next(err);
  }
}

const deleteLoadById = async (req, res, next) => {

  try {
    const load = req.load;
    const { status } = load;

    if (status !== 'NEW') {
      return next({
        message: `You can't update load with status ${status}`,
        status: 400
      })
    }

    await load.deleteOne();
    res.json({message: "Load deleted successfully"});

  } catch (err) {
    next(err);
  }
}

const postLoadById = async (req, res, next) => {

  try {
    const load = req.load;
    const { status, dimensions, payload } = load;

    if (status !== 'NEW') {
      return next({
        message: `Load have been ${status.toLowerCase()}`,
        status: 400
      })
    }
    await load.updateOne({$set: {status: 'POSTED'}});

    const assignTracks = await Truck.find({ status: 'IS'});

    const acceptableTrucksType = truckTypes.filter(truck => {
      return truck.payload >= payload;
    })
    .filter(truck => {
      for (let size in truck.dimensions) {
        return truck.dimensions[size] >= dimensions[size];
      }
    })
    .map(truck => truck.type);

    const result = assignTracks.find(truck => {
      return acceptableTrucksType.find(truckType => {
        return truck.type === truckType;
      })
    });

    if (!assignTracks.length || !result) {

      await load.updateOne({status: 'NEW'})

      return next({
        message: 'No truck found',
        status: 404
      })
    }

    await load.updateOne({$set: {status: 'ASSIGNED', assigned_to: result.assigned_to, state: 'En route to Pick Up'}});
    await Truck.findByIdAndUpdate({_id: result._id}, {status: 'OL'});   

    res.json({
      message: "Load posted successfully",
      driver_found: true
    });

  } catch (err) {
    next(err);
  }
}

const getShipperLoadInfoById = async (req, res, next) => {

  try {
    const load = req.load;
    const { assigned_to, status } = load;

    if (status === 'NEW') {
      return next({
        message: `Load hasn't assigned`,
        status: 400
      })
    }

    const truck = await Truck.findOne({created_by: assigned_to, status: 'OL'});

    res.json({load, truck});

  } catch (err) {
    next(err);
  }
}

module.exports = {
  getLoads,
  addLoads,
  getActiveLoad,
  changeActiveLoadsState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getShipperLoadInfoById
}