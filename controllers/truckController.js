const Truck = require('../models/truck');
const { truckTypeSchema } = require('../validationModels/truckValidation');

const getTracks = async (req, res, next) => {

  try {
    const { _id } = req.user;
    const trucks = await Truck.find({created_by: _id});

    if (!trucks.length) {
      return next({
        message: "No trucks found",
        status: 404
      });
    }

    res.json([{trucks}]);

  } catch (err) {
    next(err);
  }
}

const addTrack = async (req, res, next) => {

  try {
    const { type } = req.body;
    const { _id } = req.user;

    try {
      await truckTypeSchema.validateAsync({type});
    } catch (err) {
      return next({
        message: err.message,
        status: 400
      });
    }

    const truck = new Truck({created_by: _id, type, created_date: new Date().toISOString()});
    await truck.save();

    res.json({message: "Truck created successfully"})

  } catch (err) {
    next(err);
  }
}

const getTrackById = async (req, res, next) => {

  try {
    const truck = req.truck;

    res.json({truck: truck});

  } catch (err) {
    next(err);
  }
}

const updateTrackById = async (req, res, next) => {

  try {
    const truck = req.truck;
    const { assigned_to } = truck;
    const { type } = req.body;

    try {
      await truckTypeSchema.validateAsync({type});
    } catch (err) {
      return next({
        message: err.message,
        status: 400
      });
    }

    if (assigned_to !== 'null') {
      return next({
        message: 'Truck has assigned and can\'t be updated',
        status: 400
      });
    }

    await truck.updateOne({$set: {type}});
    res.json({message: "Truck details changed successfully"});

  } catch (err) {
    next(err);
  }
}

const deleteTrackById = async (req, res, next) => {

  try {
    const truck = req.truck;
    const { assigned_to } = truck;

    if (assigned_to !== 'null') {
      return next({
        message: 'Truck has assigned and can\'t be deleted',
        status: 400
      });
    }

    await truck.deleteOne();
    res.json({message: "Truck deleted successfully"});

  } catch (err) {
    next(err);
  }
}

const assignTrackById = async (req, res, next) => {

  try {
    const truck = req.truck;

    const isAssignedTruk = truck.find(truck => {
      return truck.assigned_to !== 'null'
    })

    if (!isAssignedTruk) {
      await truck.updateOne({$set: {assigned_to: created_by, status: "IS"}});
      res.json({message: "Truck assigned successfully"});
    } else {
      return next({
        message: "You've already assigned truck",
        status: 400
      });
    }

  } catch (err) {
    next(err);
  }
}

module.exports = {
  getTracks,
  addTrack,
  getTrackById,
  updateTrackById,
  deleteTrackById,
  assignTrackById
}