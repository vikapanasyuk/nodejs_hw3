const jwt = require('jsonwebtoken');
const User = require('../models/user');

const { authSchema } = require('../validationModels/authValidation');

const register = async (req, res, next) => {

  try {
    const { email, password, role } = req.body;

    try {
      await authSchema.validateAsync({email, password, role});
    } catch (err) {
      return next({
        message: err.message,
        status: 400
      });
    }
  
    const user = new User({email, password, role, created_date: new Date().toISOString()});
    await user.setPassword(password); 
    await user.save();

    res.json({message: "Profile created successfully"})

  } catch (err) {
    next(err);
  }
}

const login = async (req, res, next) => {

  try {
    const { email, password } = req.body;

    try {
      await authSchema.validateAsync({email, password});
    } catch (err) {
      return next({
        message: err.message,
        status: 400
      });
    }
    
    const user = await User.findOne({ email });
    let isValidPassword;

    if (user) {
      isValidPassword = await user.validPassword(password);
    }

    if (!user || !isValidPassword ) {
      return next({
        message: "No user with such email or password found",
        status: 404
      });
    }

    res.json({jwt_token: jwt.sign(JSON.stringify(user), process.env.SECRET)});
  
  } catch (err) {
    next(err);
  }
}

const forgotPassword = async (req, res, next) => {

  try {
    const { email } = req.body;

    try {
      await authSchema.validateAsync({email});
    } catch (err) {
      return next({
        message: err.message,
        status: 400
      });
    }

    const user = await User.findOne({ email });
  
    if (!user) {
      return next({
        message: "No user with such email found",
        status: 404
      });
    }
  
    res.json({message: "New password sent to your email address"});
  
  } catch (err) {
    next(err);
  }
}

module.exports = {
  register,
  login,
  forgotPassword
}