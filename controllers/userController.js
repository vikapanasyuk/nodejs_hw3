const User = require('../models/user');

const getUserInfo = async (req, res, next) => {

  try {
    const { _id } = req.user;

    const user = await User.findById(_id);

    if (!user) {
      return next({
        message: "No user found",
        status: 404
      });
    }

    const { _id: id, email, created_date } = user;

    res.json({id, email, created_date});
    
  } catch (err) {
    next(err);
  }
}

const deleteUserProfile = async (req, res, next) => {

  try {
    const { _id } = req.user;

    const user = await User.findByIdAndDelete(_id);

    if (!user) {
      return next({
        message: "No user found",
        status: 404
      });
    }

    res.json({message: "Profile deleted successfully"});
    
  } catch (err) {
    next(err);
  }
}

const changeUserPassword = async (req, res, next) => {

  try {
    const { oldPassword, newPassword } = req.body;
    const { _id } = req.user;
    
    const user = await User.findById(_id);

    if (!user) {
      return next({
        message: "No user found",
        status: 404
      });
    }
    const validPass = await user.validPassword(oldPassword);
    
    if (!validPass) {
      return next({
        message: "Wrong old password",
        status: 400
      });
    }

    await user.updateOne({$set: {password: newPassword}});
    await user.setPassword(newPassword);
    await user.save();
    
    res.json({message: "Password changed successfully"});

  } catch (err) {
    next(err);
  }
}

module.exports = {
  getUserInfo,
  deleteUserProfile,
  changeUserPassword
}