const express = require('express');
const router = express.Router();
const errorHandlerMiddleware = require('../middlewares/errorMiddleware');

const { 
  register,
  login,
  forgotPassword 
} = require('../controllers/authController');

router.post('/register', register, errorHandlerMiddleware);
router.post('/login', login, errorHandlerMiddleware);
router.post('/forgot_password', forgotPassword, errorHandlerMiddleware);

module.exports = router;