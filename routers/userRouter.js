const express = require('express');
const router = express.Router();

const { getUserInfo, deleteUserProfile, changeUserPassword} = require('../controllers/userController');

const authMiddleware = require('../middlewares/authMiddleware');
const errorHandlerMiddleware = require('../middlewares/errorMiddleware');


router.get('/', authMiddleware, getUserInfo, errorHandlerMiddleware);
router.delete('/', authMiddleware, deleteUserProfile, errorHandlerMiddleware);
router.patch('/password', authMiddleware, changeUserPassword, errorHandlerMiddleware);

module.exports = router;