const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware')
const errorHandlerMiddleware = require('../middlewares/errorMiddleware');
const checkDriverRoleMiddleware = require('../middlewares/checkDriverRoleMiddleware');
const checkShipperRoleMiddleware = require('../middlewares/checkShipperRoleMiddleware');
const accessMiddleware = require('../middlewares/accessMiddleware');
const resolveLoad = require('../middlewares/resolveLoad');

const { 
  getLoads,
  addLoads,
  getActiveLoad,
  changeActiveLoadsState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getShipperLoadInfoById
} = require('../controllers/loadController');

router.param('id', resolveLoad);

router.get('/', authMiddleware, getLoads, errorHandlerMiddleware);
router.post('/', authMiddleware, checkShipperRoleMiddleware, addLoads, errorHandlerMiddleware);
router.get('/active', authMiddleware, checkDriverRoleMiddleware, getActiveLoad, errorHandlerMiddleware);
router.patch('/active/state', authMiddleware, checkDriverRoleMiddleware, changeActiveLoadsState, errorHandlerMiddleware);
router.get('/:id', authMiddleware, getLoadById, errorHandlerMiddleware);
router.put('/:id', authMiddleware, checkShipperRoleMiddleware, accessMiddleware, updateLoadById, errorHandlerMiddleware);
router.delete('/:id', authMiddleware, checkShipperRoleMiddleware, accessMiddleware, deleteLoadById, errorHandlerMiddleware);
router.post('/:id/post', authMiddleware, checkShipperRoleMiddleware, postLoadById, errorHandlerMiddleware);
router.get('/:id/shipping_info', authMiddleware, checkShipperRoleMiddleware, getShipperLoadInfoById, errorHandlerMiddleware);

module.exports = router;