const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware')
const errorHandlerMiddleware = require('../middlewares/errorMiddleware');
const checkDriverRoleMiddleware = require('../middlewares/checkDriverRoleMiddleware');
const accessMiddleware = require('../middlewares/accessMiddleware');
const resolveTruck = require('../middlewares/resolveTruck');

const { 
  getTracks,
  addTrack,
  getTrackById,
  updateTrackById,
  deleteTrackById,
  assignTrackById
} = require('../controllers/truckController');

router.param('id', resolveTruck);

router.get('/', authMiddleware, checkDriverRoleMiddleware, getTracks, errorHandlerMiddleware);
router.post('/', authMiddleware, checkDriverRoleMiddleware, addTrack, errorHandlerMiddleware);
router.get('/:id', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, getTrackById, errorHandlerMiddleware);
router.put('/:id', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, updateTrackById, errorHandlerMiddleware);
router.delete('/:id', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, deleteTrackById, errorHandlerMiddleware);
router.post('/:id/assign', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, assignTrackById, errorHandlerMiddleware);

module.exports = router;