const Joi = require('joi');

const userRole = [ 'SHIPPER', 'DRIVER' ]

const authSchema = Joi.object({
  email: Joi.string()
            .email(),
  password: Joi.string(),
  role: Joi.string()
           .valid(...userRole)
});

module.exports = {
  authSchema
}