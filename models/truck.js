const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const truckSchema = new Schema({
  created_by: {
    type: String,
    required: true
  },
  assigned_to: {
    type: String,
    required: true,
    default: 'null'
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
    default: 'OS'
  },
  created_date: {
    type: String,
    required: true
  }
});

const truck = mongoose.model('truck', truckSchema);

module.exports = truck;