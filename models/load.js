const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loadSchema = new Schema({
  created_by: {
    type: String,
    required: true
  },
  assigned_to: {
    type: String,
    required: true,
    default: ' '
  },
  status: {
    type: String,
    required: true,
    default: 'NEW'
  },
  state: {
    type: String,
    required: true,
    default: ' '
  },
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: {
      type: Number,
      required: true
    },
    length: {
      type: Number,
      required: true
    },
    height: {
      type: Number,
      required: true
    }
  },
  logs: [
    {
      message: {
        type: String,
        required: true
      },
      time: {
        type: String,
        required: true
      }
    }
  ],
  created_date: {
    type: String,
    required: true
  }
});

const load = mongoose.model('load', loadSchema);

module.exports = load;