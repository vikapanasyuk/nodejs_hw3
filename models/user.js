const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true
  },
  created_date: {
    type: String,
    required: true
  }
});

const saltRounds = 10;

userSchema.methods.setPassword = async function(password) { 
  const hash = await bcrypt.hash(password, saltRounds);
  this.password = hash;
}; 

userSchema.methods.validPassword = async function(password) { 
  return bcrypt.compare(password, this.password);
};

const user = mongoose.model('user', userSchema);

module.exports = user;