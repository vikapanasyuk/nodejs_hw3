const express = require('express');
const mongoose = require('mongoose');
const app = express();

const { port } = require('./config/server');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');
const logMiddleware = require('./middlewares/logMiddleware');
require('dotenv').config();

mongoose.connect(process.env.MONGOURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

app.use(express.json())
app.use(logMiddleware);
app.use('/api/users/me', userRouter);
app.use('/api/auth', authRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.listen(port, () => {
  console.log(`Server is running on ${port} port`)
})